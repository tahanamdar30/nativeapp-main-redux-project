import React from 'react';
import store from './Redux/Store/store';
import GetTokenPage from './Component/Pages/getTokenPage';
import Profile from './Component/Pages/Profile';
import ProjectsListPage from './Component/Pages/ProjectsList';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Provider} from 'react-redux';

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerStyle: {backgroundColor: '#301B3F'},
            headerTintColor: 'white',
            headerTitleAlign: 'center',
          }}>
          <Stack.Screen
            name="Home"
            component={GetTokenPage}
            options={{title: '', statusBarHidden: true}}
          />
          <Stack.Screen
            name="Profile"
            component={Profile}
            options={{title: 'GitLab Profile', statusBarHidden: true}}
          />
          <Stack.Screen
            name="Projects"
            component={ProjectsListPage}
            options={{title: 'GitLab Projects', statusBarHidden: true}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
