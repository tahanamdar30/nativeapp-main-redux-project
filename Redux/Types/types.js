export const USER_API = 'USERAPI';
export const FORM_INPUT = 'FORMINPUT';
export const ERROR = 'ERROR';
export const PROJECTS_API = 'PROJECTSAPI';
export const ISSUES_API = 'ISSUESAPI';
