import {PROJECTS_API} from '../Types/types';

const initialState = {
  projectList: [],
};

export const reducerTwo = (state = initialState, action) => {
  switch (action.type) {
    case PROJECTS_API:
      return {...state, projectList: action.payload};
    default:
      return state;
  }
};
