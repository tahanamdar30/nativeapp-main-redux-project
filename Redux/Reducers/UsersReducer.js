import {ERROR, USER_API, FORM_INPUT} from '../Types/types';

const initialState = {
  formInput: '',
  userList: [],
  flag: true,
  error: null,
};

export const reducerOne = (state = initialState, action) => {
  switch (action.type) {
    case USER_API:
      return {...state, userList: action.payload, flag: true};
    case FORM_INPUT:
      return {...state, formInput: action.payload};
    case ERROR:
      return {...state, userList: [], error: action.payload};
    default:
      return state;
  }
};
