import {USER_API} from '../Types/types';
import {FORM_INPUT} from '../Types/types';
import {ERROR} from '../Types/types';
export const usersAction = users => {
  return {
    type: USER_API,
    payload: users,
  };
};

export const formActions = eValue => {
  return {
    type: FORM_INPUT,
    payload: eValue,
  };
};

export const errorAction = err => {
  return {
    type: ERROR,
    payload: err,
  };
};
