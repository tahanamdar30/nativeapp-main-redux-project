import {PROJECTS_API} from '../Types/types';

export const ProjectsAction = input => {
  return {
    type: PROJECTS_API,
    payload: input,
  };
};
