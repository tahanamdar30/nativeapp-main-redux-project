import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
//my Reducers
import {reducerOne} from '../Reducers/UsersReducer';
import {reducerTwo} from '../Reducers/projectsReducer';

const rootReducer = combineReducers({
  usersReducer: reducerOne,
  projectsReducer: reducerTwo,
});

const middlewere = [thunk];

const store = createStore(rootReducer, applyMiddleware(...middlewere));

export default store;
