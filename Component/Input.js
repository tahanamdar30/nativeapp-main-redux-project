import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {useDispatch, connect} from 'react-redux';
import {getUsers, formActions} from '../Redux';
import {useNavigation} from '@react-navigation/native';
//Start
const Input = ({MyState}) => {
  //Redux Hook
  const dispatch = useDispatch();
  //Input Handler
  const onChange = textValue => {
    dispatch(formActions(textValue));
  };
  //Nav Hook
  const navigation = useNavigation();
  //Handle send Data
  const handleButton = () => {
    if (MyState.formInput === '') {
      alert('Enter your token is require');
    } else {
      dispatch(getUsers());
      navigation.navigate('Profile');
    }
  };

  return (
    <ScrollView>
      <View>
        <Text style={styles.title}>Welcome !</Text>

        <View style={styles.all}>
          <View style={styles.container}>
            <Text style={styles.detail}>
              If You want to check your Projects , you should send token with
              clicking button
            </Text>

            <TextInput
              placeholder="Enter Token"
              style={styles.input}
              onChangeText={onChange}
            />
            <TouchableOpacity style={styles.btn} onPress={handleButton}>
              <Text style={styles.text}>
                Send Token {''}
                <Icon name="send" size={18} />
              </Text>
            </TouchableOpacity>
            <View>
              {MyState.error ? (
                <Text style={styles.error}>Error !! {MyState.error}</Text>
              ) : null}
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

//Styles
const styles = StyleSheet.create({
  title: {
    color: 'white',
    fontSize: 24,
    padding: 20,
    marginTop: 50,
    fontWeight: 'bold',
  },

  detail: {
    padding: 10,
    marginBottom: 20,
    fontSize: 14,
  },

  all: {
    marginTop: 20,
  },
  container: {
    padding: 25,
    paddingTop: 60,
    backgroundColor: '#eee',
    height: 600,
    borderRadius: 30,
  },

  input: {
    borderBottomWidth: 3,
    borderColor: '#93329E',
    padding: 20,
    backgroundColor: 'white',
    marginBottom: 5,
    fontSize: 16,
  },
  error: {
    backgroundColor: 'red',
    marginTop: -85,
  },
  btn: {
    backgroundColor: '#93329E',
    justifyContent: 'center',
    textAlign: 'center',
    flexDirection: 'row',
    padding: 10,
    margin: 8,
    borderWidth: 1,
    borderColor: '#93329E',
    width: 150,
    alignSelf: 'flex-end',
    marginTop: 30,
  },

  text: {color: 'white', fontSize: 18, fontWeight: 'bold'},
});
//End
const mapStateToProps = state => {
  return {
    MyState: state.usersReducer,
  };
};

export default connect(mapStateToProps)(Input);
