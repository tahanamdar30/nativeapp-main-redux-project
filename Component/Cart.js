import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {connect, useDispatch} from 'react-redux';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {useNavigation} from '@react-navigation/native';
import {getProjects} from '../API/projectsApi';
const Cart = ({MyState}) => {
  console.log(MyState.userList);
  //Nav Hook
  const navigation = useNavigation();
  //UseDispatch
  const dispatch = useDispatch();

  //nextButtonHandler
  const nextHandler = () => {
    dispatch(getProjects());
    navigation.navigate('Projects');
  };

  return (
    <View style={styles.containers}>
      <Text style={styles.text}>{MyState.userList.name}</Text>
      <Image
        source={{
          uri: `${MyState.userList.avatar_url}`,
        }}
        style={styles.image}
      />
      <TouchableOpacity style={styles.btn} onPress={nextHandler}>
        <Text style={styles.nextPage}>
          Next {''}
          <Icon name="space-shuttle" size={30} />
        </Text>
      </TouchableOpacity>
      <Text style={styles.text2}>
        <Icon name="gitlab" size={18} color="white" /> {''}
        Welcome {''}
        <Text style={styles.span}>{MyState.userList.name}</Text>
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  containers: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
  },
  text: {
    color: 'white',
    fontSize: 22,
    borderBottomWidth: 1,
    borderColor: '#93329E',
    fontWeight: 'bold',
  },
  text2: {
    marginTop: 30,
    fontSize: 22,
    color: '#93329E',
    fontWeight: 'bold',
    borderBottomWidth: 1,
    borderColor: 'white',
  },

  image: {
    width: 100,
    height: 100,
    marginTop: 60,
    borderRadius: 100 / 1,
  },
  span: {
    color: 'white',
    fontSize: 20,
  },

  nextPage: {
    backgroundColor: '#93329E',
    width: 120,
    height: 40,
    color: 'white',
    fontSize: 25,

    fontWeight: 'bold',
    textAlign: 'center',
  },
  btn: {borderRadius: 10, marginTop: 30},
});

const mapStateToProps = state => {
  return {
    MyState: state.usersReducer,
  };
};

export default connect(mapStateToProps)(Cart);
