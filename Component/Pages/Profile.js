import React from 'react';
import {View, StyleSheet} from 'react-native';
import Cart from '../Cart';
const Profile = () => {
  return (
    <View style={styles.body}>
      <Cart />
    </View>
  );
};

const styles = StyleSheet.create({
  body: {
    backgroundColor: '#301B3F',
    height: '100%',
  },
});

export default Profile;
