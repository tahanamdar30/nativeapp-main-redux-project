import React from 'react';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

//React-Native
import {
  View,
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import {connect} from 'react-redux';

const ProjectsListPage = ({MyState1}) => {
  return (
    <ScrollView style={{backgroundColor: '#301B3F'}}>
      <View style={styles.container}>
        <FlatList
          data={MyState1.projectList}
          renderItem={({item}) => (
            <TouchableOpacity style={styles.ListItem}>
              <View style={styles.ListItemChild}>
                <Text style={styles.font}>{item.name}</Text>
                <Icon name="inbox" size={20} color="white" />
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  ListItem: {
    padding: 25,
    borderBottomWidth: 1,
    borderColor: '#301B3F',
    backgroundColor: '#93329E',
    color: 'white',
    width: 350,
    alignSelf: 'center',
    borderRadius: 10,
    marginTop: 25,
  },

  ListItemChild: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    textAlign: 'center',
  },
  font: {
    fontSize: 20,
    color: 'white',
  },
});
const mapStateToProps = state => {
  return {
    MyState1: state.projectsReducer,
  };
};

export default connect(mapStateToProps)(ProjectsListPage);
