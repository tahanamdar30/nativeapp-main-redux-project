import axios from 'axios';
import store from '../Redux/Store/store';
import {errorAction, usersAction} from '../Redux/Actions/UserActions';
// import AsyncStorage from '@react-native-async-storage/async-storage';
export const getUsers = () => async dispatch => {
  try {
    const token = store.getState().usersReducer.formInput;
    const {data} = await axios.get('https://gitlab.com/api/v4/user', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    dispatch(usersAction(data));
  } catch (error) {
    const errorMsg = error.message;
    dispatch(errorAction(errorMsg));
  }
};
