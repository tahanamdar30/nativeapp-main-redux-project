import axios from 'axios';
import store from '../Redux/Store/store';
import {ProjectsAction} from '../Redux/Actions/ProjectActions';

// API REQUEST
export const getProjects = () => async dispatch => {
  try {
    const userList = store.getState().usersReducer.userList;
    const {data} = await axios.get(
      `https://gitlab.com/api/v4/users/${userList.id}/projects`,
      {},
    );
    console.log(data);
    dispatch(ProjectsAction(data));
  } catch (error) {
    console.log(error);
  }
};
